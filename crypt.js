const bcrypt = require('bcrypt');

function hash(data) {
  console.log("hashing data");

  return bcrypt.hashSync(data,10);
}

function checkPassword(sentPassword, userHashPassword){
  console.log("Checking password");

  return bcrypt.compareSync(sentPassword,userHashPassword);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
