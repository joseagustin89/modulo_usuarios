const express = require('express');

require('dotenv').config();

const app = express();



const userController = require('./controlers/UserController');

//para que la aplicacion en el body parsee Json es la siguiente linea
app.use(express.json());

//para permitir rutas de distintos dominios

var enableCORS = function(req, res, next) {
 // No producción!!!11!!!11one!!1!
 res.set("Access-Control-Allow-Origin", "*");
 res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
 res.set("Access-Control-Allow-Headers", "Content-Type");
 next();
}
app.use(enableCORS);

//para que la aplicacion escuche en el puerto 3000
app.listen(3000);

app.post("/apitechu/v2/users", userController.createUserV2);

app.get("/apitechu/v2/users", userController.getUsersV2);

app.get("/apitechu/v2/users/:id", userController.getUserByIdV2);


app.get("/apitechu/v1/hello",
function (req, res) {
  console.log("hello world");
  res.send({"msg" : "hello world"});
}
);
